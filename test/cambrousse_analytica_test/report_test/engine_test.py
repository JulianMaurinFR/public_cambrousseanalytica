from cambrousse_analytica.report.register import REPORT_CONF
from cambrousse_analytica.report.engine import Engine
from cambrousse_analytica_test.helpers import get_conversation
from cambrousse_analytica.business.report import Report


def test_engine_default():
    document = get_conversation('hamlet-a1-s1.html')
    engine = Engine([document])
    report = engine.run('default')
    assert isinstance(report, Report)
    print(report.serialize())
