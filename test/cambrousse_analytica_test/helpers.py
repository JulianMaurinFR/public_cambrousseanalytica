import os
import pytest

def data_dir():
    if 'CAMBROUSSE_ANALYTICA_HOME' not in os.environ:
        raise Exception('Missing CAMBROUSSE_ANALYTICA_HOME environment')
    return os.path.join(
        os.environ['CAMBROUSSE_ANALYTICA_HOME'],
        'ressources', 'test', 'data'
    )

def get_conversation(filename):
    filepath = os.path.join(data_dir(), 'conversations', filename)
    with open(filepath, 'r') as stream:
        conversation = stream.read()
    return conversation
