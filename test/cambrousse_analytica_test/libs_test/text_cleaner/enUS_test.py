import pytest
from cambrousse_analytica.context import CambrousseAnalyticaContext
from cambrousse_analytica.libs.text_cleaner import TextCleaner

@pytest.fixture
def cleaner():
    context = CambrousseAnalyticaContext.instance()
    context.language_tag = 'en-US'
    return TextCleaner()

def test_remove_white(cleaner):
    text = 'hello world\nhello'
    expected = 'helloworldhello'
    assert cleaner.clean_white(text) == 'helloworldhello'
