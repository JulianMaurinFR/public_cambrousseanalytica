from cambrousse_analytica_test.helpers import get_conversation
from cambrousse_analytica.libs.conversation_parser import ConversationParser

def test_parse_hamlet():
    conversation = get_conversation('hamlet-a1-s1.html')
    conversation = ConversationParser(conversation).parse()
    assert len(conversation.messages) == 12
    assert 'FRANCISCO' in conversation.participants
    assert 'BARNARDO' in conversation.participants

