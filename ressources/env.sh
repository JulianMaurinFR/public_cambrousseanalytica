# --------------------------------------------------------------------------- #
#                      CAMBROUSSE ANALYTICA ENV SCRIPT                        #
# --------------------------------------------------------------------------- #
#                                                                             #
# Append followings lines to your .bashrc :                                   #
#                                                                             #
#       export  CAMBROUSSE_ANALYTICA_HOME="{PATH TO THE PROJECT}"             #
#       source $CAMBROUSSE_ANALYTICA_HOME/ressources/env.sh                   #
#                                                                             #
# --------------------------------------------------------------------------- #

if [ -z "$CAMBROUSSE_ANALYTICA_HOME" ]; then
    echo "MISSING CAMBROUSSE_ANALYTICA_HOME ENVIRONMENT"
    exit 1
fi

PATHS=(
    'src/'
    'test/'
)
for path in ${PATHS[@]}
do
    export PYTHONPATH="$CAMBROUSSE_ANALYTICA_HOME/$path:$PYTHONPATH"
done
