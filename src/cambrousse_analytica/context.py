
class CambrousseAnalyticaContext:

    _INSTANCE = None

    @classmethod
    def instance(cls):
        if cls._INSTANCE is None:
            cls._INSTANCE = CambrousseAnalyticaContext()
        return cls._INSTANCE
    

    def __init__(self):
        self.text_language_tag = 'en-US'

