from bs4 import BeautifulSoup
from cambrousse_analytica.business.conversation import Conversation, Message
from cambrousse_analytica.exception import InvalidConversation

class ConversationParser:

    def __init__(self, html_conversation):
        try:
            self.soup = BeautifulSoup(html_conversation, 'html.parser')
        except Exception as ex:
            raise InvalidConversation('BeautifulSoup loading failure')

    def _is_message(self, node):
        if not node.name == "div":
            return False
        try:
            return (str(node['class']) == "['message']")
        except:
            return False

    def parse(self):
        # try:
        return self._parse()
        # except Exception as ex:
            # raise InvalidConversation('Parsing error : {}'.format(ex.message) )
   
    def _parse(self):
        title = self.soup.head.title.text
        conversation = Conversation(title)
        #
        theads = self.soup.find_all("div", class_="thread")
        for thread in theads:
            message = None
            for node in thread:
                if self._is_message(node):
                    if message:
                        conversation.messages.append(message)
                    user = node.find_all("span", class_="user")[0].text
                    meta = node.find_all("span", class_="meta")[0].text
                    message = Message(user, meta)
                if message and node.name == "p":
                    message.add_text(node.text)
            if message:
                conversation.messages.append(message)
        return conversation
