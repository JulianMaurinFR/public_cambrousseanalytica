import re
from cambrousse_analytica.context import CambrousseAnalyticaContext

class TextCleaner:
    
    def __init__(self):
        self.language_tag = CambrousseAnalyticaContext.instance().text_language_tag

    def clean_white(self, text):
        return re.sub(r'\s', '', text)

    def clean(cls, text):
        # TODO
        return text