#!/usr/bin/env python
# coding: utf-8
'''
CambrousseAnalytica Main
'''
import sys, os
import json
import tempfile
import uuid

# -----------------------------------

def usage():
    print('Usage: ./main.py [path to conversation]')

if __name__ == '__main__':
    from cambrousse_analytica.report.register import REPORT_CONF
    from cambrousse_analytica.report.engine import Engine
    #
    if len(sys.argv) < 2 or not os.path.isfile(sys.argv[1]) :
        usage()
        sys.exit(1)
    #
    with open(sys.argv[1], 'r') as stream:
        document = stream.read()
    # Run default report
    engine = Engine([document], REPORT_CONF['default'])
    report = engine.run()
    #
    output_path = os.path.join(tempfile.gettempdir(), '{}.html'.format(uuid.uuid4()))
    with open(output_path, 'w+') as stream:
        stream.write(json.dumps(report.serialize, indent=4, sort_keys=True))
    print('Report generated: {}'.format(output_path))

