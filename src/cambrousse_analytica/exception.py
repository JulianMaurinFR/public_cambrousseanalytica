
class InvalidConversation(Exception):
    def __init__(self, source):
        Exception.__init__(self, "This file is not a valid conversation")
        self.source = source
