from cambrousse_analytica.business.report_entry import ReportEntry

class Statistic(ReportEntry):
    def __init__(self, value, title="", unit=""):
        ReportEntry.__init__(self, title)
        self.value = value
        self.unit = unit
        self.register_serializer_attributes(['value'])