from cambrousse_analytica.business.serializer import Serializer
from cambrousse_analytica.libs.text_cleaner import TextCleaner

class Message(Serializer):
    def __init__(self, author='unknow', date='unknow', text=''):
        Serializer.__init__(self)
        self.author = author
        self.date = date
        self.original_text = text
        self._text = ''
        self.register_serializer_attributes([
            'author', 'text', 'date'
        ])
    
    def add_text(self, text):
        self.original_text += text

    @property
    def text(self):
        if not self._text:
            self._text = TextCleaner().clean(self.original_text)
        return self._text
    


class Conversation(Serializer):
    def __init__(self, title):
        Serializer.__init__(self)
        self.title = title
        # 
        self._messages = []
        self._participants = []
        self._full_text = ''
        self._words = []
        self._message_by_author = None
        #
        self.register_serializer_attributes([
            'title', 'messages'
        ])

    @property
    def messages(self):
        return self._messages

    @property
    def participants(self):
        if not self._participants:
            self._participants = list(set([msg.author for msg in self.messages]))
        return self._participants
    
    @property
    def full_text(self):
        if not self._full_text:
            for msg in self.messages:
                self._full_text += " " + msg.text
        return self._full_text

    @property
    def words(self):
        if not self._words:
            self._words = self.full_text.split(' ')
        return self._words
    
    @property
    def message_by_author(self):
        if not self._message_by_author:
            self._message_by_author = {}
            for message in self.messages:
                if message.author not in self._message_by_author:
                    self._message_by_author[message.author] = []
                self._message_by_author[message.author].append(message)
        return self._message_by_author
