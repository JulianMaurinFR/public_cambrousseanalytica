import pygal
from cambrousse_analytica.business.report_entry import ReportEntry

class Chart(ReportEntry):
    def __init__(self, title='', unit=''):
        ReportEntry.__init__(self, title, unit)
        self.image_b64 = None
        self.register_serializer_attributes(['base64'])
    
    @property
    def base64(self):
        return self.chart.render_data_uri()


class LineChart(Chart):
    def __init__(self, title='', unit=''):
        Chart.__init__(self, title, unit)
        self.chart = pygal.Line(
            show_legend=False,
            width=450,
            height=200,
            margin=0
        )
