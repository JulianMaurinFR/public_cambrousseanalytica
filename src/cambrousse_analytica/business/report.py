from cambrousse_analytica.business.serializer import Serializer
from cambrousse_analytica.business.statistic import Statistic
from cambrousse_analytica.business.chart import Chart
from cambrousse_analytica.business.ranking import Ranking


class Report(Serializer):

    def __init__(self):
        Serializer.__init__(self)
        self.statistics = []
        self.charts = []
        self.rankings = []
        self.register_serializer_attributes([
            'statistics', 'charts', 'rankings'
        ])

    def append(self, report_entry, title):
        report_entry.title = title
        if isinstance(report_entry, Statistic):
            self.statistics.append(report_entry)
        elif isinstance(report_entry, Chart):
            self.charts.append(report_entry)
        elif isinstance(report_entry, Ranking):
            self.rankings.append(report_entry)
        else:
            raise Exception('Invalid report entry')
