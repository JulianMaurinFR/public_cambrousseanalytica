import datetime
import dateparser
from cambrousse_analytica.business.serializer import Serializer

class ConversationDatetime:
    """
    Datetime manipulation on conversation 
    """
    
    def __init__(self, messages):
        self.messages = messages
        #
        self._messages_datetime = []
        self._datetime_range = None
        self._minutes_series = None
    
    def _parse_datetime(self, text_datetime):
        # Split timzone
        text = text_datetime[:-7]
        # Parse
        return dateparser.parse(text)

    def _add_month(self, date):
        return (
            datetime.date(year=date.year+1, day=date.day, month=1)
            if date.month == 12 else
            datetime.date(year=date.year, day=date.day, month=date.month+1)
        )

    @property
    def datetime_messages(self):
        if not self._messages_datetime:
            for msg in self.messages:
                self._messages_datetime.append(self._parse_datetime(msg.date))
        return self._messages_datetime
    
    @property
    def datetime_range(self):
        if not self._datetime_range:
            self._datetime_range = (min(self.datetime_messages), max(self.datetime_messages))
        return self._datetime_range

    def build_series(self):
        """
        Build the most appropriate series (month,day,hour,minutes) 
        base on range
        """
        min_datetime, max_datetime = self.datetime_range
        delta = (max_datetime - min_datetime)
        # 
        if delta.days > 31:
            return self._build_month_series()
        if delta.days > 0:
            return self._build_days_series()
        if delta.seconds > 7200: # 2 hours
            return self._build_hours_series()
        return self._build_minutes_series()

    def _build_minutes_series(self):
        if not self._minutes_series:
            # Times
            min_datetime, max_datetime = self.datetime_range
            min_datetime = datetime.datetime(
                year=min_datetime.year, day=min_datetime.day, month=min_datetime.month,
                hour=min_datetime.hour, minute=min_datetime.minute, second=0
            )
            max_datetime = datetime.datetime(
                year=max_datetime.year, day=max_datetime.day, month=max_datetime.month,
                hour=max_datetime.hour, minute=max_datetime.minute, second=0
            )
            times_series = []
            times_series.append(min_datetime)
            while times_series[-1] != max_datetime:
                t = times_series[-1] + datetime.timedelta(seconds=60)
                times_series.append(t)
            # Values
            values_series = []
            for t in times_series:
                score = 0
                for msg_datetime in self.datetime_messages:
                    if (msg_datetime - t).seconds < 60:
                        score += 1
                values_series.append(score)
            # Times to label
            labels = [ "{0:02d}:{1:02d}".format(t.hour, t.minute) for t in times_series ]
            self._minutes_series = (labels, values_series)
        return self._minutes_series
        


    # @property
    # def datetime_series_month(self):
    #     if not self._datetime_series_month:
    #         min_datetime, max_datetime = self.datetime_range
    #         min_date = datetime.date(year=min_datetime.year, day=1, month=min_datetime.month)
    #         max_date = datetime.date(year=max_datetime.year, day=1, month=max_datetime.month)
    #         #
    #         self._datetime_series_month.append(min_date)
    #         while self._datetime_series_month[-1] != max_date:
    #             t = self._add_month(self._datetime_series_month[-1])
    #             self._datetime_series_month.append(t)
    #     return self._datetime_series_month

