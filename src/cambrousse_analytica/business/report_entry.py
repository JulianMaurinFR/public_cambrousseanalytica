from cambrousse_analytica.business.serializer import Serializer

class ReportEntry(Serializer):
    def __init__(self, title="", unit=""):
        Serializer.__init__(self)
        self.unit = unit
        self.title = title
        self.register_serializer_attributes([
            'title', 'unit'
        ])