
class Serializer:

    def __init__(self):
        self.serializer_attributes = []

    def register_serializer_attributes(self, attrs):
        self.serializer_attributes += attrs
    
    def __repr__(self):
        return str(self.serialize())

    def serialize(self):
        res = {}
        for attr in set(self.serializer_attributes):
            res[attr] = getattr(self, attr)
        return res
