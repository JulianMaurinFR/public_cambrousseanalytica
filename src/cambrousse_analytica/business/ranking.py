import operator
from cambrousse_analytica.business.report_entry import ReportEntry

class Ranking(ReportEntry):

    def __init__(self, title, unit, limit):
        ReportEntry.__init__(self, title, unit)
        self.limit = limit
        self.register_serializer_attributes(['ranking'])
    
    def add(self, key, value):
        if key not in self._ranking:
            self._ranking[key] = 0
        self._ranking[key] += value
    
    def sort(self):
        raise NotImplementedError()

    @property
    def ranking(self):
        return self.sort()

class TopRanking(Ranking):
    """
    descending ranking
    """

    def __init__(self, title='', unit='', limit=100):
        Ranking.__init__(self, title, unit, limit)
        self._ranking = {}
    
    def sort(self):
        res = []
        for i in range(self.limit):
            max_key, max_value = max(self._ranking.items(), key=operator.itemgetter(1))
            if 0 == self._ranking[max_key]:
                break
            res.append({'key': max_key, 'value': max_value})
            self._ranking[max_key] = 0
        return res

class FlopRanking(Ranking):
    """
    ascending ranking
    """

    def __init__(self, title='', unit='', limit=100):
        Ranking.__init__(self, title, unit, limit)
        self._ranking = {}
    
    def sort(self):
        res = []
        for i in range(self.limit):
            min_key, min_value = min(self._ranking.items(), key=operator.itemgetter(1))
            if -1 == self._ranking[min_key]:
                break
            res.append({'key': min_key, 'value': min_value})
            self._ranking[min_key] = -1
        return res
    