"""
    cf. Engine.py
"""

from cambrousse_analytica.report.statistic import count as count_statistic
from cambrousse_analytica.report.ranking import flood as flood_rankings
from cambrousse_analytica.report.chart import  times_series as times_series_chats

"""
    The register references all methods generating report entry 
"""

class Register:
    def __init__(self):
        self.register = {
            'STATISTIC': {},
            'RANKING': {},
            'CHART': {}
        }
    
    def add(self, category, title, func, args=False):
        self.register[category].update({
            title: (func, args)
        })
    
    def add_statistic(self, title, func, args=False):
        self.add('STATISTIC', title, func, args=False)
    
    def add_ranking(self, title, func, args=False):
        self.add('RANKING', title, func, args=False)
    
    def add_chart(self, title, func, args=False):
        self.add('CHART', title, func, args=False)

    def all(self):
        res = {}
        res.update(self.register['STATISTIC'])
        res.update(self.register['RANKING'])
        res.update(self.register['CHART'])
        return res

REGISTER = Register()
# Statistics
REGISTER.add_statistic('Messages count', count_statistic.messages_count)
REGISTER.add_statistic('Words count', count_statistic.words_count)
REGISTER.add_statistic('Word occurances count', count_statistic.word_occurances_count)
# Rankings
REGISTER.add_ranking('Best participants by number of messages', flood_rankings.top_message_flood_ranking)
REGISTER.add_ranking('Best participants by text quantiy', flood_rankings.top_text_flood_ranking)
# Charts
REGISTER.add_chart('Messages over time', times_series_chats.message_over_time)


"""
    Provides report configuration
"""
REPORT_CONF = {
    'default': {
        'STATISTIC':[
            ('Messages count', []),
            ('Words count', []),
        ],
        'RANKING':[
            ('Best participants by number of messages', []),
            ('Best participants by text quantiy', []),
        ],
        'CHART':[
            ('Messages over time', []),
        ],
    }
}

