from cambrousse_analytica.business.statistic import Statistic

def words_count(conversation):
    return Statistic(value=len(conversation.words))

def messages_count(conversation):
    return Statistic(value=len(conversation.messages))

# Args

def word_occurances_count(conversation, word):
    return Statistic(value=conversation.words.count(word))
