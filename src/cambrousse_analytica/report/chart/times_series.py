import datetime
from cambrousse_analytica.business.chart import LineChart
from cambrousse_analytica.business.conversation_datetime import ConversationDatetime


def message_over_time(conversation):
    # Get series
    conversation_datetime = ConversationDatetime(conversation.messages)
    labels, values = conversation_datetime.build_series()
    # Build chart
    line_chart = LineChart()
    line_chart.chart.x_labels = labels
    line_chart.chart.add('messages', values)
    return line_chart
