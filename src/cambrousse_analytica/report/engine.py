from cambrousse_analytica.libs.conversation_parser import ConversationParser
from cambrousse_analytica.business.report import Report
from cambrousse_analytica.report.register import REGISTER, REPORT_CONF
# from cambrousse_analytica.exception import RegisterError

class Engine:
    """
    __init__ args
        documents: HTML conversations as text list
        report_entries: Functions list to execute to generate the report
            Function is a tuple (function label, function args )
            eg. [('Messages count', []), ('Word occurances count', [hello])]
    
    The objective of this class is to bind function label and arguments
    from report_entries to the right function to execute. 
    This is possible by the referencing of the methods in a register 
    (cf. register.py)

    In this way we keep the possibility of building personalized reports 
    according to a user selection.

    The inconvenient is the need to add each new function by hand.
    An optimization would be desirable, but must respect the constraints
    of evolutivity and must bring a concrete improvement 
        -> a fancy way would be to provide a decorator like @statistic
           who will wrap the result into Statistic object
    """

    def __init__(self, documents, report_entries=None):
        self.documents = documents
        self.report_entries = report_entries
        self.conversations = []
        for document in self.documents:
            self.conversations.append(ConversationParser(document).parse())


    def get_method(self, label):
        for (method_label, method) in REGISTER.all().items():
            if label == method_label:
                return method

    def run(self, conf=None):
        if conf:
            # Run based on configuration
            if conf not in REPORT_CONF:
                raise Exception('Conf {} does not found'.format(conf))
            conf = REPORT_CONF[conf]
            entries = {}
            entries.update(conf['STATISTIC'])
            entries.update(conf['RANKING'])
            entries.update(conf['CHART'])
            self.report_entries = entries
        #
        report = Report()
        for (label, args) in self.report_entries.items():
            method, has_args = self.get_method(label)
            if method:
                for conversation in self.conversations:
                    report.append(method(conversation, *args), label)
        return report
