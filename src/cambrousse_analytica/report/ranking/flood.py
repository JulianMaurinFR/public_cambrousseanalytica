from cambrousse_analytica.business.ranking import TopRanking
from cambrousse_analytica.libs.text_cleaner import TextCleaner

def top_message_flood_ranking(conversation):
    ranking = TopRanking(unit='messages count')
    for author, messages in conversation.message_by_author.items():
        ranking.add(author, len(messages))
    return ranking

def top_text_flood_ranking(conversation):
    cleaner = TextCleaner()
    ranking = TopRanking(unit='characters')
    for author, messages in conversation.message_by_author.items():
        text = cleaner.clean_white(' '.join([msg.original_text for msg in messages]))
        ranking.add(author, len(text))
    return ranking
