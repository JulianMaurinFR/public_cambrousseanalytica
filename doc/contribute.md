# Cambrousse Analytica how to contribute

## Github

Fork the open source project [CambrousseAnalytica](https://github.com/JulianMaurin/CambrousseAnalytica).

When your code is ready send my a merge request.
If the code is merged the result will be available on [Cambrousse-Analytica.ovh](https://Cambrousse-Analytica.ovh)


Reading the architecture documentation is a good way to approach the project.

Of course, don't forget to develop the tests associated with your code !


## Quick start

You can contribute to the project simply by adding new report methods.

Three types of statistics are available:

* __Statistics:__ Simple title and value couple. (eg. the count of all words of the conversation)
* __Ranking:__ Mutiples title and value couples sorted (eg. ranking of participations by number of messages )
* __Chart:__ A chart image generated with `pygal` ([pygal.org](http://pygal.org))


### Create your method

Let's add your method to the corresponding python file. (eg. `src/cambrousse_analytica/report/statistic/count.py`)


    # The methods used to generate the report always
    # take the conversation as the first argument
    # ( cf. doc/images/uml_conversation.gif and src/cambrousse_analytica/business/conversation.py)
    def messages_count(conversation):
        # Also the output shall be a wrapper (inherite from ReportEntry) from business package
        # A ReportEntry concret class is available for all types of statistics (Statistic, Ranking and Chart)
        return Statistic(value=len(conversation.words))


### Register your methode

Once your method is ready, add it a to the register `src/cambrousse_analytica/report/register.py` :

    # import method
    from cambrousse_analytica.report.statistic import count as count_statistic

    # Add it to the register with an human-readable label as key
    REGISTER = {
        # [...]
        'Messages count': count_statistic.messages_count,
        # [...]

