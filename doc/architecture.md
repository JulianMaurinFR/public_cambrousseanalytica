# Cambrousse Analytica architecture

## Overview 

    Cambrousse_analytica
    ├── report
    ├── libs
    └── business

----------

## Report

This package includes all statistics generation operations

    Cambrousse_analytica
    └── report
        ├── engine.py
        ├── register.py
        ├── ranking
        ├── chart
        └── statistic

* __engine & register:__ Report generation engine.
* __ranking & chart & statistic:__ Statistics generation

----------

## Libs

Independent software brick, meets a specific need

    Cambrousse_analytica
    └── libs
       ├── conversation_parser.py
       └── text_cleaner.py

* __conversation_parser:__ Parse HTML conversation file
* __text_cleaner:__ Text cleaning for analysis 

----------

## Business

Conversation manipulation framework

    Cambrousse_analytica
    └── business
       ├── conversation.py
       ├── conversation_datetime.py
       ├── serializer.py
       ├── report.py
       ├── report_entry.py
       ├── chart.py
       ├── ranking.py
       └── statistic.py

* __conversation:__ Conversation API object (cf. doc/images/uml_conversation.gif)
* __conversation_datetime:__ Conversation datetime manipulation and series generation
* __serializer:__ Allows easy serialization of objects 
* __report:__ A high level class containing the report
* __report_entry:__ Abstract class defining the elements of the report
* __chart & ranking & statistic:__ Concrete elements of the report